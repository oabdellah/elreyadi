(function() {
    var swipey2 = {
        slideContainer: null, //<ul> element object that holds the image slides
        wrapper: null, //meant for masking/clipping
        slides: null, //array of all slides i.e <li> elements
        distanceX: 0, //distance moved in X direction i.e left or right
        startX: 0, //registers the initial touch co-ordinate
        preferredWidth: 0, //dynamic variable to set width
        preferredHeight: 0, //dynamic variable to set height
        direction: "", //direction of movement
        timer: null, //timer that set starts when touch starts
        timerCounter: 0, //counter variable for timer
        isTouchStart: false, //boolen to chk whether touch has started
        maxDistance: 0, //maximum distance in X direction that slide container can move
        currentDistance: 0, //current distance moved by slide container through translate

        initSwipey2: function() {
            //scroll the window up to hide the address bar of the browser.
            window.setTimeout(function() {
                window.scrollTo(0, 1);
            }, 100);
            //get all the instances of the HTML elements
            swipey2.wrapper = document.getElementById("wrapper2");
            
           

            //for iPhone, the width and height
            swipey2.preferredWidth = window.innerWidth;
            swipey2.preferredHeight = window.innerHeight; //510 for android
            //setting the width and height to our wrapper with overflow = hidden

            //setting the width to our <ul> element which holds all the <li> elements

            //calculating the max distance of travel for Slide Container i.e <ul> element
            swipey2.maxDistance = swipey2.preferredWidth;
           
            //initialize and assign the touch events
            swipey2.initEvents();
        },
        initEvents: function() {
            //registering touch events to the wrapper
            swipey2.wrapper.addEventListener("touchstart", swipey2.startHandler, false);
            swipey2.wrapper.addEventListener("touchmove", swipey2.moveHandler, false);
            swipey2.wrapper.addEventListener("touchend", swipey2.endHandler, false);
			//swipey2.wrapper.addEventListener("click", function(){alert("fhjfjfjfj");return false;}, false);
        },
        //funciton called when touch start event is fired i.e finger is pressed on the screen
        startHandler: function(event) {
           
            //stores the starting X co-ordinate when finger touches the device screen
            swipey2.startX = event.touches[0].pageX; //.changedTouches[0]
		
            //timer is set on
            swipey2.timer = setInterval(function() {
                swipey2.timerCounter++;
            }, 10);
            swipey2.isTouchStart = true;
		
		
		
           
        },
        //funciton called when touch move event is fired i.e finger is dragged over the screen
        moveHandler: function(event) {
			
				
            if (swipey2.isTouchStart) {
				//event.preventDefault(); //prevents the window from scrolling.
                swipey2.distanceX = event.touches[0].pageX - swipey2.startX;
			
            //move the slide container along with the movement of the finger
            // swipey2.slideContainer.style.webkitTransform = "translate3d(" + (swipey2.distanceX + swipey2.currentDistance) + "px, 0,0)";
            }
				
        },
        //funciton called when touch end event is fired i.e finger is released from screen
        endHandler: function(event) {
        //  alert(event.changedTouches.length);
            clearInterval(swipey2.timer); //timer is stopped
            if (swipey2.distanceX > 0) {
                swipey2.direction = "right";
                
            }
            if (swipey2.distanceX < 0) {
                swipey2.direction = "left";
                
            }
			
			
		
            //the following conditions have been discussed in details
            //            if ((swipey2.direction == "right" && swipey2.currentDistance == 0) || (swipey2.direction == "left" && swipey2.currentDistance == -(swipey2.maxDistance - swipey2.preferredWidth))) {
            //                alert(3);
            //                swipey2.comeBack();
            //            }
            //            else
            if (swipey2.timerCounter < 30 && swipey2.distanceX > 10) {
              
                swipey2.moveRight();
                
            }
            else if (swipey2.timerCounter < 30 && swipey2.distanceX < -10) {
                
                swipey2.moveLeft();
            }
            else if (swipey2.distanceX <= -(swipey2.preferredWidth / 2)) { //-160
               
                swipey2.moveLeft();
            }
            else if (swipey2.distanceX >= (swipey2.preferredWidth / 2)) { //160
                
                swipey2.moveRight();
            }
            else {
				
				//function preventDefault(event) {
//    event.preventDefault();
//}
			//	$('#wrapper2').unbind(event.preventDefault());
			
		//	$('body').unbind(function(event){event.preventDefault();});
		//$('#wrapper2').bind('click');

          //     alert("gggg");
			//	document.addEventListener("click", function (e) {  }, false);
			
                swipey2.comeBack();
            }

            swipey2.timerCounter = 0; //reset timerCounter
            swipey2.isTouchStart = false; //reset the boolean var
            swipey2.distanceX = 0; //reset the distance moved for next iteration
        },
        moveLeft: function() {
           
            next();
        
        },
        moveRight: function() {
            
           pre();
         
        },
        comeBack: function() {
		//	return false;
        //            swipey2.slideContainer.style.webkitTransitionDuration = 250 + "ms";
        //            swipey2.slideContainer.style.webkitTransitionTimingFunction = "ease-out";
        //            swipey2.slideContainer.style.webkitTransform = "translate3d(" + swipey2.currentDistance + "px, 0,0)";
        }
    }; //end of swipey2 object
    window.swipey2Obj = swipey2; //expose to global window object
})();
swipey2Obj.initSwipey2(); //invoke the init method to get started