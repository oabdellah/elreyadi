(function() {
    var items;
    var weeks;
    var swipey = {
        slideContainer: null, //<ul> element object that holds the image slides
        wrapper: null, //meant for masking/clipping
        slides: null, //array of all slides i.e <li> elements
        distanceX: 0, //distance moved in X direction i.e left or right
        startX: 0, //registers the initial touch co-ordinate
        preferredWidth: 0, //dynamic variable to set width
        preferredHeight: 0, //dynamic variable to set height
        direction: "", //direction of movement
        timer: null, //timer that set starts when touch starts
        timerCounter: 0, //counter variable for timer
        isTouchStart: false, //boolen to chk whether touch has started
        maxDistance: 0, //maximum distance in X direction that slide container can move
        currentDistance: 0, //current distance moved by slide container through translate
        nextCount :1,

        initSwipey: function() {
            //scroll the window up to hide the address bar of the browser.
            window.setTimeout(function() {
                window.scrollTo(0, 1);
            }, 100);
            //get all the instances of the HTML elements
            swipey.wrapper = document.getElementById("wrapper");
            swipey.slideContainer = document.getElementById("slideContainer");
            swipey.slides = slideContainer.getElementsByTagName("li");
            
            
            weeks=30;
            var scrollPage=3;
            
            items= parseInt(weeks/scrollPage);
            var group = items * scrollPage;
            
         
//            var remain=0;
//            var shown= weeks;
//            if(group < weeks){
//                 remain =3- (weeks - group);
//                 shown= weeks+remain;
//            }
//            
            
            var l=document.getElementById("slideContainer");
                for (var i=2; i<= weeks ; i++){
                    
                    l.innerHTML=l.innerHTML+'<li id="l'+i+'"><div id="s'+i+'" class="non-high-light" onclick="move('+i+')"></div></li>'
                }
//                for (var j= (weeks+1); j<= (weeks + remain) ; j++){
//                   
//                    l.innerHTML=l.innerHTML+'<li><div class="non-high-light" ><label class="counter" style="color:lightgray">'+j+'</label></div></li>'
//                }
             var h= $('.non-high-light').height();        
                $('.non-high-light').css('width',h+'px'); 
                $('.high-light').css('width',h+'px');
                  
                 
                  
                var w=$('#slideContainer li').width();
                var m= window.innerWidth*.01;
               
                var temp;
                
                
                temp= w*scrollPage + m*(scrollPage+1);
                $('#wrapper').css('width',temp+'px'); 
                
                
                temp= w*weeks + m*(weeks+1);
                $('#slideContainer').css('width',temp+'px');
                $('#slideContainer li').css('margin-left',m+'px');
                  
            //for iPhone, the width and height
            swipey.preferredWidth = $('#wrapper').width()- m ;
         
          
            swipey.preferredHeight = $('#wrapper').height(); //510 for android
            //setting the width and height to our wrapper with overflow = hidden

            //swipey.wrapper.style.width = swipey.preferredWidth + "px";
            //swipey.wrapper.style.height = swipey.preferredHeight + "px";

            //setting the width to our <ul> element which holds all the <li> elements
            //
            //swipey.slideContainer.style.width = swipey.slides.length * swipey.preferredWidth + "px";
            //swipey.slideContainer.style.height = swipey.preferredHeight + "px";
            //
            //
            //calculating the max distance of travel for Slide Container i.e <ul> element
            
           // items = shown/3;
           if(group < weeks){
               items= items +1;
           }
           
           swipey.maxDistance =items * swipey.preferredWidth  ;
//           var remain= weeks % 3;
//           var smallScrol =remain * (h) ;
      
        //  swipey.maxDistance =(items * swipey.preferredWidth )+ smallScrol ;
          
          
        //   swipey.maxDistance  = localStorage.getItem('weeks');
          // alert(swipey.maxDistance)
            //initialize and assign the touch events
            swipey.initEvents();
        },
        initEvents: function() {
            //registering touch events to the wrapper
            swipey.wrapper.addEventListener("touchstart", swipey.startHandler, false);
            swipey.wrapper.addEventListener("touchmove", swipey.moveHandler, false);
            swipey.wrapper.addEventListener("touchend", swipey.endHandler, false);
        },
        //funciton called when touch start event is fired i.e finger is pressed on the screen
        startHandler: function(event) {
            
            $('#slideContainer li').show();
            //stores the starting X co-ordinate when finger touches the device screen
            swipey.startX = event.touches[0].pageX; //.changedTouches[0]
            //timer is set on
            swipey.timer = setInterval(function() {
                swipey.timerCounter++;
            }, 10);
            swipey.isTouchStart = true;
            event.preventDefault(); //prevents the window from scrolling.
        },
        //funciton called when touch move event is fired i.e finger is dragged over the screen
        moveHandler: function(event) {
            if (swipey.isTouchStart) {
                swipey.distanceX = event.touches[0].pageX - swipey.startX;
                //move the slide container along with the movement of the finger
                swipey.slideContainer.style.webkitTransform = "translate3d(" + (swipey.distanceX + swipey.currentDistance) + "px, 0,0)";
            }
        },
        //funciton called when touch end event is fired i.e finger is released from screen
        endHandler: function(event) {
            clearInterval(swipey.timer); //timer is stopped
            if (swipey.distanceX > 0) {
                swipey.direction = "right";
            }
            if (swipey.distanceX < 0) {
                swipey.direction = "left";
            }
			
            //the following conditions have been discussed in details
            if ((swipey.direction == "right" && swipey.currentDistance == 0) || (swipey.direction == "left" && swipey.currentDistance == -(swipey.maxDistance - swipey.preferredWidth))) {
                swipey.comeBack();
                
            }
            else if (swipey.timerCounter < 30 && swipey.distanceX > 10) {
                swipey.moveRight();
               
            }
            else if (swipey.timerCounter < 30 && swipey.distanceX < -10) {
                swipey.moveLeft();
              
            }
            else if (swipey.distanceX <= -(swipey.preferredWidth / 2)) { //-160
                swipey.moveLeft();
                
            }
            else if (swipey.distanceX >= (swipey.preferredWidth / 2)) { //160
                swipey.moveRight();
              
            }
            else {
				
                swipey.comeBack();
                
            }

            swipey.timerCounter = 0; //reset timerCounter
            swipey.isTouchStart = false; //reset the boolean var
            swipey.distanceX = 0; //reset the distance moved for next iteration
        },
        moveLeft: function() {
            
            
//            alert(swipey.nextCount);
//          if(items == swipey.nextCount){
//                
//                var remainItems = weeks % 3;
//               
//                var smallScroll= (56+window.innerWidth*.01) *remainItems;
//                swipey.preferredWidth = smallScroll;
//               
//            }
//             alert(swipey.preferredWidth)
            swipey.currentDistance += -swipey.preferredWidth;
            swipey.slideContainer.style.webkitTransitionDuration = 300 + "ms";
            //using CSS3 transformations - translate3d function for movement
            swipey.slideContainer.style.webkitTransform = "translate3d(" + swipey.currentDistance + "px, 0,0)";
            var cur =localStorage.getItem('scroller');
            cur =parseInt(cur);
             $('#s'+cur).attr('class','non-high-light');
             
            var divid=cur/3;
            var result =(divid)-parseInt(divid);
           
            var step;
            if(result ==0)  
                {
                  step=2;  
                }
                else if(result > .6){
                    step =3;
                }
                else if(result > .3 ){
                    step =4;
                }
                
                
               
            cur = cur + step;
            
//            if(cur <= weeks){
//             $('#s'+cur).attr('class','high-light');
//            
//            }else{
//                cur= weeks;
//                $('#s'+cur).attr('class','high-light');
//            }

             $('#s'+cur).attr('class','high-light');
            localStorage.setItem('scroller', cur);
            sched();
           
          //  swipey.nextCount= swipey.nextCount+1;
        },
        moveRight: function() {
            swipey.currentDistance += swipey.preferredWidth;
            swipey.slideContainer.style.webkitTransitionDuration = 300 + "ms";
            swipey.slideContainer.style.webkitTransform = "translate3d(" + swipey.currentDistance + "px, 0,0)";
            var cur =localStorage.getItem('scroller');
            cur =parseInt(cur);
           
            $('#s'+cur).attr('class','non-high-light');
             var divid=cur/3;
            var result =(divid)-parseInt(divid);
            var step;
            if(result ==0)  
                {
                  step=4;  
                }
                 else if(result > .6){
                    step =3;
                }
                else if(result > .3 ){
                    step =2;
                }
                
            cur = cur - step;
           
             $('#s'+cur).attr('class','high-light');
            localStorage.setItem('scroller', cur);
            sched();
        },
        comeBack: function() {
            swipey.slideContainer.style.webkitTransitionDuration = 250 + "ms";
            swipey.slideContainer.style.webkitTransitionTimingFunction = "ease-out";
            swipey.slideContainer.style.webkitTransform = "translate3d(" + swipey.currentDistance + "px, 0,0)";
        }
    }; //end of swipey object
    window.swipeyObj = swipey; //expose to global window object
})();
swipeyObj.initSwipey(); //invoke the init method to get started