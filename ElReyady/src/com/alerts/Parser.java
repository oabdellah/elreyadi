package com.alerts;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.databseSettings.All_Matches;
import com.databseSettings.All_Matches_Desc;
import com.databseSettings.Fav_Match_Desc;
import com.databseSettings.Favorite_Match;

import android.util.Log;

public class Parser {

	public ArrayList<String> min5_matches = new ArrayList<String>();
	public ArrayList<String> description = new ArrayList<String>();
	public ArrayList<String> goals = new ArrayList<String>();

	public String getXmlFromUrl(String url) {
		String xml = null;

		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			xml = EntityUtils.toString(httpEntity);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// return XML

		// parse(xml);
		ArrayList<All_Matches_Desc> m = parse_All_Matches_Desc(xml);
		int j = m.size();
		// parse_All_Matches(xml);
		// parse_All_Matches_Desc(xml);
		// parse_Fav_Match_Desc(xml);
		return xml;
	}

	public String getValue(Element item, String str) {
		NodeList n = item.getElementsByTagName(str);
		return this.getElementValue(n.item(0));
	}

	public final String getElementValue(Node elem) {
		Node child;
		if (elem != null) {
			if (elem.hasChildNodes()) {
				for (child = elem.getFirstChild(); child != null; child = child
						.getNextSibling()) {
					if (child.getNodeType() == Node.TEXT_NODE) {
						return child.getNodeValue();
					}
				}
			}
		}
		return "";
	}
	
	public String status(String xml) {

		String reslt = null;

		if (!(xml.equals(""))) {
			Document doc = getDomElement(xml); // getting DOM element
			NodeList nl = doc.getElementsByTagName("matches_api");

			Element e = (Element) nl.item(0);
			
			reslt=getValue(e, "favorite_team");
		}

		return reslt;
	}

	public ArrayList<Favorite_Match> parse_Favorite_Match(String xml) {

		ArrayList<Favorite_Match> reslt = new ArrayList<Favorite_Match>();

		if (!(xml.equals(""))) {

			Document doc = getDomElement(xml); // getting DOM element
			NodeList nl = doc.getElementsByTagName("Match");

			for (int i = 0; i < nl.getLength(); i++) {

				Element e = (Element) nl.item(i);
				Favorite_Match m = new Favorite_Match();

				m.setId(getValue(e, "ID"));
				m.setFirst_Team(getValue(e, "First_Team"));
				m.setSecond_Team(getValue(e, "Second_Team"));
				m.setTime_Stamp(getValue(e, "Time_Stamp"));

				reslt.add(m);

			}
		}

		return reslt;
	}

	public ArrayList<Fav_Match_Desc> parse_Fav_Match_Desc(String xml) {

		ArrayList<Fav_Match_Desc> reslt = new ArrayList<Fav_Match_Desc>();

		if (!(xml.equals(""))) {
			Document doc = getDomElement(xml); // getting DOM element
			NodeList nl = doc.getElementsByTagName("Match");

			for (int i = 0; i < nl.getLength(); i++) {

				Element e = (Element) nl.item(i);
				Fav_Match_Desc m = new Fav_Match_Desc();

				m.setId(getValue(e, "ID"));
				m.setDes_Name(getValue(e, "Des_Name"));
				m.setSound(getValue(e, "Sound"));
				m.setTime_stamp(getValue(e, "Time_stamp"));

				reslt.add(m);

			}
		}

		return reslt;
	}

	public ArrayList<All_Matches> parse_All_Matches(String xml) {

		ArrayList<All_Matches> reslt = new ArrayList<All_Matches>();

		if (!(xml.equals(""))) {
			Document doc = getDomElement(xml); // getting DOM element
			NodeList nl = doc.getElementsByTagName("Match");

			for (int i = 0; i < nl.getLength(); i++) {

				Element e = (Element) nl.item(i);
				All_Matches m = new All_Matches();

				m.setId(getValue(e, "ID"));
				m.setFirst_Team(getValue(e, "First_Team"));
				m.setSecond_Team(getValue(e, "Second_Team"));
				m.setTime_Stamp(getValue(e, "Time_Stamp"));
				
				reslt.add(m);

			}
		}

		return reslt;
	}

	public ArrayList<All_Matches_Desc> parse_All_Matches_Desc(String xml) {

		ArrayList<All_Matches_Desc> reslt = new ArrayList<All_Matches_Desc>();

		if (!(xml.equals(""))) {
			Document doc = getDomElement(xml); // getting DOM element
			NodeList nl = doc.getElementsByTagName("Match");

			for (int i = 0; i < nl.getLength(); i++) {

				Element e = (Element) nl.item(i);
				All_Matches_Desc m = new All_Matches_Desc();

				m.setId(getValue(e, "ID"));
				m.setDes_Name(getValue(e, "Des_Name"));
				m.setSound(getValue(e, "Sound"));
				m.setTime_stamp(getValue(e, "Time_stamp"));

				reslt.add(m);

			}
		}

		return reslt;
	}

	public Document getDomElement(String xml) {
		Document doc = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {

			DocumentBuilder db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			doc = db.parse(is);

		} catch (ParserConfigurationException e) {
			Log.e("Error: ", e.getMessage());
			return null;
		} catch (SAXException e) {
			Log.e("Error: ", e.getMessage());
			return null;
		} catch (IOException e) {
			Log.e("Error: ", e.getMessage());
			return null;
		}
		// return DOM
		return doc;
	}
}
