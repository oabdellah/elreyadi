package com.elreyady;

import org.apache.cordova.DroidGap;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.view.WindowManager;

import com.alerts.BootReciever;
import com.alerts.DeviceDataControl;
import com.alerts.RunNotification;

public class NewsActivity  extends DroidGap {
	public static String url = "";
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DeviceDataControl dv_coControl = new DeviceDataControl(this);

		String d = DeviceDataControl.getDEVICE_ID();
	

		getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		Check_Account ok= new Check_Account(this);
		boolean temp = ok.isFirst();
		if(temp == true){
			url = "file:///android_asset/www/start.html?device_id="+d;
			
		}else{
			url = "file:///android_asset/www/News.html";
		}
		super.loadUrl(url);

		Intent i = new Intent(this, BootReciever.class);
		sendBroadcast(i);
		clearNotifications();
		
		

	}
	
	
	public void clearNotifications() {
		NotificationManager mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		mgr.cancel(RunNotification.NOTIFICATION_NUMBER);
		RunNotification.Count = 0;

	}

	
	

}
